package main

import (
	"github.com/tecbot/gorocksdb"
	"fmt"
	"net/http"
	"math/rand"
	"io/ioutil"
	"strconv"
	"log"
	"github.com/go-redis/redis"
	"runtime"
)
//CGO_CFLAGS="-I/home/ivanovdiit/src/rocksdb/include" CGO_LDFLAGS="-L/home/ivanovdiit/src/rocksdb/ -lrocksdb -lstdc++ -lm -lz -lbz2 -lsnappy -llz4 -lzstd" go run main.go
func main() {
	maxInt := 100000
	runtime.GOMAXPROCS(4)
	bbto := gorocksdb.NewDefaultBlockBasedTableOptions()
	bbto.SetBlockCache(gorocksdb.NewLRUCache(3 << 20))
	opts := gorocksdb.NewDefaultOptions()
	opts.SetBlockBasedTableFactory(bbto)
	opts.SetCreateIfMissing(true)
	db, err := gorocksdb.OpenDb(opts, "/home/ivanovdiit/temp/rockdbdb/")
	defer db.Close()
	if err != nil {
		fmt.Println(err.Error())
	}


	wo := gorocksdb.NewDefaultWriteOptions()
	//err = db.Put(wo, []byte("foo"), []byte("bar"))
	//if err != nil {
	//	fmt.Println(err.Error())
	//}

	ro := gorocksdb.NewDefaultReadOptions()
	//value, err := db.Get(ro, []byte("foo"))
	//fmt.Println(string(value.Data()))
	//defer value.Free()
	//
	//err = db.Delete(wo, []byte("foo"))


	redis_client := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
		MaxRetries: 5,
	})




	putData, err := ioutil.ReadFile("/home/ivanovdiit/go/src/gorocksdb/data.txt")

	if err != nil {
		fmt.Println(err.Error())
	}

	fmt.Println(string(putData))

	http.HandleFunc(
		"/",
		func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintf(w, "LOAD GOROCKSDB")
		})

	http.HandleFunc(
		"/generateRand",
		func(w http.ResponseWriter, r *http.Request) {
			ran := rand.Intn(maxInt)
			err := db.Put(wo, []byte(strconv.Itoa(ran)), putData)
			if err != nil {
				fmt.Println(err.Error())
			}
			fmt.Println("Success add ", strconv.Itoa(ran))
			fmt.Fprintf(w, "Success add %d", ran)
		})

	http.HandleFunc(
		"/readRand",
		func(w http.ResponseWriter, r *http.Request) {
			ran := rand.Intn(maxInt)
			data, err := db.Get(ro, []byte(strconv.Itoa(ran)))
			if err != nil {
				fmt.Println(err.Error())
			}
			fmt.Println("Success get ", string(data.Data()))
			fmt.Fprintf(w, "Success get %s", string(data.Data()))
			data.Free()
		})

	http.HandleFunc(
		"/read",
		func(w http.ResponseWriter, r *http.Request) {

			keys, ok := r.URL.Query()["key"]

			if !ok || len(keys[0]) < 1 {
				log.Println("Url Param 'key' is missing")
				return
			}

			// Query()["key"] will return an array of items,
			// we only want the single item.
			key := keys[0]

			data, err := db.Get(ro, []byte(key))
			if err != nil {
				fmt.Println(err.Error())
			}
			fmt.Println("Success get ", string(data.Data()))
			fmt.Fprintf(w, "Success get %s", string(data.Data()))
		})


	http.HandleFunc(
		"/readAndWriteRand",
		func(w http.ResponseWriter, r *http.Request) {
			ran := rand.Intn(maxInt)
			err := db.Put(wo, []byte(strconv.Itoa(ran)), putData)
			if err != nil {
				fmt.Println(err.Error())
			}
			fmt.Println("Success add ", strconv.Itoa(ran))
			fmt.Fprintf(w, "Success add %d", ran)

			ran = rand.Intn(maxInt)
			data, err := db.Get(ro, []byte(strconv.Itoa(ran)))
			if err != nil {
				fmt.Println(err.Error())
			}
			fmt.Println("Success get ", string(data.Data()))
			fmt.Fprintf(w, "Success get %s", string(data.Data()))
			data.Free()
		})





	http.HandleFunc(
		"/generateRandRedis",
		func(w http.ResponseWriter, r *http.Request) {
			ran := rand.Intn(maxInt)
			status := redis_client.Set(strconv.Itoa(ran), putData, 0)
			if status.Err() != nil {
				fmt.Println(status.Err().Error())
			}
			fmt.Println("Success redis add ", strconv.Itoa(ran))
			fmt.Fprintf(w, "Success redis add %d", ran)
		})

	http.HandleFunc(
		"/readRandRedis",
		func(w http.ResponseWriter, r *http.Request) {
			ran := rand.Intn(maxInt)
			cmd := redis_client.Get(strconv.Itoa(ran))
			data, err := cmd.Result()
			if err != nil {
				fmt.Println(err.Error())
			}

			fmt.Println("Success redis get ", data)
			fmt.Fprintf(w, "Success redis get %s", data)
		})

	http.HandleFunc(
		"/readAndWriteRandRedis",
		func(w http.ResponseWriter, r *http.Request) {
			ran := rand.Intn(maxInt)
			status := redis_client.Set(strconv.Itoa(ran), putData, 0)
			if status.Err() != nil {
				fmt.Println(status.Err().Error())
			}
			fmt.Println("Success redis add ", strconv.Itoa(ran))
			fmt.Fprintf(w, "Success redis add %d", ran)

			ran = rand.Intn(maxInt)
			cmd := redis_client.Get(strconv.Itoa(ran))
			data, err := cmd.Result()
			if err != nil {
				fmt.Println(err.Error())
			}

			fmt.Println("Success redis get ", data)
			fmt.Fprintf(w, "Success redis get %s", data)
		})


	http.HandleFunc(
		"/readRedis",
		func(w http.ResponseWriter, r *http.Request) {

			keys, ok := r.URL.Query()["key"]

			if !ok || len(keys[0]) < 1 {
				log.Println("Url Param 'key' is missing")
				return
			}

			// Query()["key"] will return an array of items,
			// we only want the single item.
			key := keys[0]

			cmd := redis_client.Get(key)
			data, err := cmd.Result()
			if err != nil {
				fmt.Println(err.Error())
			}

			fmt.Println("Success redis get ", data)
			fmt.Fprintf(w, "Success redis get %s", data)
		})




	err = http.ListenAndServe(":8088", nil)
	if err != nil {
		panic("cannot listen: " + err.Error())
	}


}
